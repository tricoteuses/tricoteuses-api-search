# Tricoteuses-API-Search

## _API web de recherche pour Tricoteuses basée sur les données open data du Parlement_

## Installation

TODO

## Utilisation

### Indexation des données de l'Assemblée

Cette indexation utilise le [service GraphQL _Tricoteuses_ des données de l'Assemblée nationale](https://framagit.org/tricoteuses/tricoteuses-api-assemblee/).

```bash
rm -Rf data/
cargo run index codes
cargo run index dossiers
```

### Lancement du serveur web


```bash
cargo run serve
```

Pour tester le service, ouvrir l'URL `http://localhost:8000` dans un navigateur.
