use clap::ArgMatches;
use std::fs::{self, File};
use std::io::prelude::*;
use std::path::Path;
use tantivy::schema::Document;

use crate::config::Config;
use crate::codes;

fn run_index_codes(config: &Config, config_dir: &Path, data_dir: &Path) -> tantivy::Result<()> {
    {
        let index = codes::build_index(data_dir)?;
        let mut index_writer = index.writer(50_000_000)?;
        let schema = index.schema();

        let filename_field = schema.get_field("filename").unwrap();
        let content_autocomplete_field = schema.get_field("content_autocomplete").unwrap();
        let content_field = schema.get_field("content").unwrap();

        let codes_dir = config_dir.join(&config.archeo_lex.codes_dir);
        for entry in fs::read_dir(codes_dir).expect("error while reading directory of \"codes\"") {
            let path = entry.unwrap().path();
            let filename = path.file_name().unwrap().to_string_lossy();
            if filename.starts_with(".") {
                continue;
            }
            if !filename.ends_with(".md") {
                continue;
            }
            let mut markdown_file = File::open(&path).expect("JSON file not found");
            let mut markdown_text = String::new();
            markdown_file
                .read_to_string(&mut markdown_text)
                .expect("error while reading Markdown file");

            let mut code_doc = Document::default();
            code_doc.add_text(filename_field, &filename);
            code_doc.add_text(content_autocomplete_field, &markdown_text);
            code_doc.add_text(content_field, &markdown_text);
            index_writer.add_document(code_doc);
        }
        index_writer.commit()?;
    }

    Ok(())
}

pub fn run_index_codes_cli(
    config: &Config,
    config_dir: &Path,
    data_dir: &Path,
    _args: &ArgMatches,
) -> Result<(), String> {
    run_index_codes(config, config_dir, data_dir).map_err(|e| format!("Indexing failed : {:?}", e))
}
