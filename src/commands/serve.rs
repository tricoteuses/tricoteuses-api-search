use clap::ArgMatches;
use rocket::http::Method;
use rocket::State;
use rocket_contrib::json::Json;
use rocket_cors::AllowedOrigins;
use std::convert::From;
use std::path::Path;
use tantivy::collector::{Count, MultiCollector, TopDocs};
use tantivy::query::{AllQuery, QueryParser};
use tantivy::schema::{NamedFieldDocument, Schema};
use tantivy::DocAddress;
use tantivy::Document;
use tantivy::Index;

use crate::codes;
use crate::config::Config;
use crate::dossiers;

#[derive(Serialize)]
struct CodeHit {
    filename: String,
    id: u32,
}

struct CodesResource {
    autocomplete_query_parser: QueryParser,
    index: Index,
    list_query_parser: QueryParser,
    schema: Schema,
}

impl CodesResource {
    fn create_hit(&self, doc: &Document, doc_address: DocAddress) -> CodeHit {
        CodeHit {
            filename: doc.get_first(self.schema.get_field("filename").unwrap()).unwrap().text().unwrap().to_string(),
            id: doc_address.doc(),
        }
    }
}

#[derive(Serialize)]
struct CodesSearchResult {
    q: String,
    count: usize,
    hits: Vec<CodeHit>,
}

#[derive(Serialize)]
struct DossierHit {
    doc: NamedFieldDocument,
    id: u32,
}

struct DossiersResource {
    autocomplete_query_parser: QueryParser,
    index: Index,
    list_query_parser: QueryParser,
    schema: Schema,
}

impl DossiersResource {
    fn create_hit(&self, doc: &Document, doc_address: DocAddress) -> DossierHit {
        DossierHit {
            doc: self.schema.to_named_doc(&doc),
            id: doc_address.doc(),
        }
    }
}

#[derive(Serialize)]
struct DossiersSearchResult {
    q: String,
    count: usize,
    hits: Vec<DossierHit>,
}

#[get("/codes?<limit>&<q>&<offset>")]
fn list_codes(
    limit: Option<usize>,
    q: Option<String>,
    offset: Option<usize>,
    resource: State<CodesResource>,
) -> Json<CodesSearchResult> {
    let limit = limit.unwrap_or(10);
    let q = q.unwrap_or_else(|| "".to_string());
    let offset = offset.unwrap_or(0);

    let term = q.clone();
    let term = term.replace("'", " ");
    let term = term.replace(":", " ");
    let term = term.replace("\"", " ");
    let term = term.trim();

    let searcher = resource.index.searcher();
    let query = if term.is_empty() {
        Box::new(AllQuery)
    } else {
        resource
            .list_query_parser
            .parse_query(term)
            .expect("Parsing the query failed")
    };
    let mut collectors = MultiCollector::new();
    let topdocs_handler = collectors.add_collector(TopDocs::with_limit(offset + limit));
    let count_handler = collectors.add_collector(Count);
    let mut multifruits = searcher.search(&query, &collectors).unwrap();
    let docs = topdocs_handler.extract(&mut multifruits);
    let hits: Vec<CodeHit> = docs
        .iter()
        .skip(offset)
        .map(|(_, doc_address)| {
            let doc: Document = searcher.doc(*doc_address).unwrap();
            resource.create_hit(&doc, *doc_address)
        })
        .collect();
    Json(CodesSearchResult {
        q,
        count: count_handler.extract(&mut multifruits),
        hits,
    })
}

#[get("/codes/autocomplete?<limit>&<q>&<offset>")]
fn autocomplete_codes(
    limit: Option<usize>,
    q: Option<String>,
    offset: Option<usize>,
    resource: State<CodesResource>,
) -> Json<CodesSearchResult> {
    let limit = limit.unwrap_or(10);
    let q = q.unwrap_or_else(|| "".to_string());
    let offset = offset.unwrap_or(0);

    let term = q.clone();
    let term = term.replace("'", " ");
    let term = term.replace(":", " ");
    let term = term.replace("\"", " ");
    let term = term.trim();

    let searcher = resource.index.searcher();
    let query = if term.is_empty() {
        Box::new(AllQuery)
    } else {
        resource
            .autocomplete_query_parser
            .parse_query(term)
            .expect("Parsing the query failed")
    };
    let mut collectors = MultiCollector::new();
    let topdocs_handler = collectors.add_collector(TopDocs::with_limit(offset + limit));
    let count_handler = collectors.add_collector(Count);
    let mut multifruits = searcher.search(&query, &collectors).unwrap();
    let docs = topdocs_handler.extract(&mut multifruits);
    let hits: Vec<CodeHit> = docs
        .iter()
        .skip(offset)
        .map(|(_, doc_address)| {
            let doc: Document = searcher.doc(*doc_address).unwrap();
            resource.create_hit(&doc, *doc_address)
        })
        .collect();
    Json(CodesSearchResult {
        q,
        count: count_handler.extract(&mut multifruits),
        hits,
    })
}

#[get("/dossiers?<limit>&<q>&<offset>")]
fn list_dossiers(
    limit: Option<usize>,
    q: Option<String>,
    offset: Option<usize>,
    resource: State<DossiersResource>,
) -> Json<DossiersSearchResult> {
    let limit = limit.unwrap_or(10);
    let q = q.unwrap_or_else(|| "".to_string());
    let offset = offset.unwrap_or(0);

    let term = q.clone();
    let term = term.replace("'", " ");
    let term = term.replace(":", " ");
    let term = term.replace("\"", " ");
    let term = term.trim();

    let searcher = resource.index.searcher();
    let query = if term.is_empty() {
        Box::new(AllQuery)
    } else {
        resource
            .list_query_parser
            .parse_query(term)
            .expect("Parsing the query failed")
    };
    let mut collectors = MultiCollector::new();
    let topdocs_handler = collectors.add_collector(TopDocs::with_limit(offset + limit));
    let count_handler = collectors.add_collector(Count);
    let mut multifruits = searcher.search(&query, &collectors).unwrap();
    let docs = topdocs_handler.extract(&mut multifruits);
    let hits: Vec<DossierHit> = docs
        .iter()
        .skip(offset)
        .map(|(_, doc_address)| {
            let doc: Document = searcher.doc(*doc_address).unwrap();
            resource.create_hit(&doc, *doc_address)
        })
        .collect();
    Json(DossiersSearchResult {
        q,
        count: count_handler.extract(&mut multifruits),
        hits,
    })
}

#[get("/dossiers/autocomplete?<limit>&<q>&<offset>")]
fn autocomplete_dossiers(
    limit: Option<usize>,
    q: Option<String>,
    offset: Option<usize>,
    resource: State<DossiersResource>,
) -> Json<DossiersSearchResult> {
    let limit = limit.unwrap_or(10);
    let q = q.unwrap_or_else(|| "".to_string());
    let offset = offset.unwrap_or(0);

    let term = q.clone();
    let term = term.replace("'", " ");
    let term = term.replace(":", " ");
    let term = term.replace("\"", " ");
    let term = term.trim();

    let searcher = resource.index.searcher();
    let query = if term.is_empty() {
        Box::new(AllQuery)
    } else {
        resource
            .autocomplete_query_parser
            .parse_query(term)
            .expect("Parsing the query failed")
    };
    let mut collectors = MultiCollector::new();
    let topdocs_handler = collectors.add_collector(TopDocs::with_limit(offset + limit));
    let count_handler = collectors.add_collector(Count);
    let mut multifruits = searcher.search(&query, &collectors).unwrap();
    let docs = topdocs_handler.extract(&mut multifruits);
    let hits: Vec<DossierHit> = docs
        .iter()
        .skip(offset)
        .map(|(_, doc_address)| {
            let doc: Document = searcher.doc(*doc_address).unwrap();
            resource.create_hit(&doc, *doc_address)
        })
        .collect();
    Json(DossiersSearchResult {
        q,
        count: count_handler.extract(&mut multifruits),
        hits,
    })
}

fn run_serve(config: &Config, _config_dir: &Path, data_dir: &Path) -> tantivy::Result<()> {
    let codes_resource = {
        let index = codes::load_index(data_dir)?;
        let schema = index.schema();
        let content_autocomplete_field = schema.get_field("content_autocomplete").unwrap();
        let content_field = schema.get_field("content").unwrap();
        let autocomplete_query_parser =
            QueryParser::for_index(&index, vec![content_autocomplete_field]);
        let list_query_parser = QueryParser::for_index(&index, vec![content_field]);
        CodesResource {
            autocomplete_query_parser,
            index,
            list_query_parser,
            schema,
        }
    };

    let dossiers_resource = {
        let index = dossiers::load_index(data_dir)?;
        let schema = index.schema();
        let titre_autocomplete_field = schema.get_field("titre_autocomplete").unwrap();
        let titre_field = schema.get_field("titre").unwrap();
        let autocomplete_query_parser =
            QueryParser::for_index(&index, vec![titre_autocomplete_field]);
        let list_query_parser = QueryParser::for_index(&index, vec![titre_field]);
        DossiersResource {
            autocomplete_query_parser,
            index,
            list_query_parser,
            schema,
        }
    };

    // Start Rocket web server.
    let (allowed_origins, failed_origins) = AllowedOrigins::some(
        config
            .api
            .allowed_origins
            .iter()
            .map(String::as_ref)
            .collect::<Vec<&str>>()
            .as_slice(),
    );
    assert!(failed_origins.is_empty());
    let cors = rocket_cors::Cors {
        allowed_origins,
        allowed_methods: vec![Method::Get, Method::Post]
            .into_iter()
            .map(From::from)
            .collect(),
        allow_credentials: true,
        ..Default::default()
    };
    rocket::ignite()
        .attach(cors)
        // .manage(data_dir)
        .manage(codes_resource)
        .manage(dossiers_resource)
        .mount("/", routes![autocomplete_codes, list_codes, autocomplete_dossiers, list_dossiers])
        .launch();
    Ok(())
}

pub fn run_serve_cli(
    config: &Config,
    config_dir: &Path,
    data_dir: &Path,
    _args: &ArgMatches,
) -> Result<(), String> {
    run_serve(config, config_dir, data_dir).map_err(|e| format!("{:?}", e))
}
