use clap::ArgMatches;
use json;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use tantivy::schema::Document;

use crate::config::Config;
use crate::dossiers;

#[derive(Serialize)]
struct Query {
    query: String,
    variables: Variables,
}

#[derive(Serialize)]
struct Variables {
    pub legislature: i64,
}

fn run_index_dossiers(config: &Config, _config_dir: &Path, data_dir: &Path) -> tantivy::Result<()> {
    {
        let index = dossiers::build_index(data_dir)?;
        let mut index_writer = index.writer(50_000_000)?;
        let schema = index.schema();

        let uid_field = schema.get_field("uid").unwrap();
        let titre_autocomplete_field = schema.get_field("titre_autocomplete").unwrap();
        let titre_field = schema.get_field("titre").unwrap();

        let mut graphql_query_file = File::open("src/graphql_queries/dossiers_assemblee.graphql")
            .expect("GraphQL file not found");
        let mut graphql_query_string = String::new();
        graphql_query_file
            .read_to_string(&mut graphql_query_string)
            .expect("An error occurred while reading GraphQL file");
        let query = Query {
            query: graphql_query_string.to_string(),
            variables: Variables { legislature: 15 },
        };

        let client = reqwest::Client::new();
        let mut response = client
            .post(&config.assemblee.graphql_url)
            .json(&query)
            .send()
            .unwrap();
        let response_body: json::JsonValue = json::parse(&response.text().unwrap()).unwrap();
        let errors = &response_body["errors"];
        if !errors.is_null() {
            println!(
                "An error occurred when calling https://assemblee.api.tricoteuses.fr/graphql:"
            );
            for error in errors.members() {
                println!("{:?}", error.to_string());
            }
        }
        let data = &response_body["data"];
        assert!(!data.is_null());
        for dossier in data["dossiersParlementaires"].members() {
            // println!("{:#?}", dossier);
            let mut dossier_doc = Document::default();
            dossier_doc.add_text(uid_field, dossier["uid"].as_str().unwrap());
            let titre = dossier["titreDossier"]["titre"].as_str().unwrap();
            dossier_doc.add_text(titre_autocomplete_field, &titre);
            dossier_doc.add_text(titre_field, &titre);
            index_writer.add_document(dossier_doc);
        }
        index_writer.commit()?;
    }

    Ok(())
}

pub fn run_index_dossiers_cli(
    config: &Config,
    config_dir: &Path,
    data_dir: &Path,
    _args: &ArgMatches,
) -> Result<(), String> {
    run_index_dossiers(config, config_dir, data_dir).map_err(|e| format!("Indexing failed : {:?}", e))
}
