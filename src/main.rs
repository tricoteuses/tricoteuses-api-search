#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

mod codes;
mod commands;
mod config;
mod dossiers;
mod tokenizers;

use clap::{App, AppSettings, Arg, SubCommand};
use std::io::Write;
use std::path::Path;

use crate::commands::*;

fn main() {
    let cli_options = App::new("DFIH-Search")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .version(env!("CARGO_PKG_VERSION"))
        .author("Emmanuel Raviart <emmanuel@raviart.com>")
        .about(r#"Search web service for the "Tricoteuses" project"#)
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true),
        )
        .subcommand(
            SubCommand::with_name("index")
                .about("Index data")
                .subcommand(SubCommand::with_name("codes").about(r#"Index Archéo Lex "codes""#))
                .subcommand(SubCommand::with_name("dossiers").about(r#"Index "dossiers" collected by requesting Tricoteuses web services"#)),
        )
        .subcommand(SubCommand::with_name("serve").about("Start web server"))
        .get_matches();

    let config_file_path = Path::new(cli_options.value_of("config").unwrap_or("../Config.toml"));
    let config = config::load(config_file_path);

    let config_dir = config_file_path.parent().unwrap();
    let data_dir = config_dir.join(&config.data.dir);

    let (subcommand, some_options) = cli_options.subcommand();
    let mut options = some_options.unwrap();
    let run_cli = match subcommand {
        "index" => {
            let (level2_sub_command, some_level2_options) = options.subcommand();
            options = some_level2_options.unwrap();
            match level2_sub_command {
                "codes" => run_index_codes_cli,
                "dossiers" => run_index_dossiers_cli,
                _ => panic!(r#"Subcommand "index {}" is unknown"#, level2_sub_command),
            }
        },
        "serve" => run_serve_cli,
        _ => panic!("Subcommand {} is unknown", subcommand),
    };

    if let Err(ref e) = run_cli(&config, config_dir, &data_dir, options) {
        let stderr = &mut std::io::stderr();
        let errmsg = "Error writing ot stderr";
        writeln!(stderr, "{}", e).expect(errmsg);
        std::process::exit(1);
    }
}
