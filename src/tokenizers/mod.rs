mod french_stop_words;
mod slugifier;

pub use self::french_stop_words::FRENCH_STOP_WORDS;
pub use self::slugifier::Slugifier;
